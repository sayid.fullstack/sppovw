<?php
include '../../../sppovw/vendor/autoload.php';
include '../../api/config/db_connection.php';

$DBQueryObj = new DBQuery($host, $username, $password, $database_name);

$sqlQueryObj = new SQLQuery($DBQueryObj);
$sqlQueryObj->setSELECT_INNER_JOIN_Query('peserta','peserta_id','maklumbalas','peserta_id');
$sqlQueryObj->addReturnField('peserta.peserta_id as id');
$sqlQueryObj->addReturnField('peserta_nama');
$sqlQueryObj->addReturnField('DATE_FORMAT(maklumbalas_date,\'%d/%m/%Y %r\') AS maklumbalas_date ');
$sqlQueryObj->addConditionField('kursus_id',1,IFieldType::INTEGER_TYPE,IConditionOperator::NONE);
$DBQueryObj->setSQL_Statement($sqlQueryObj->getSQLQuery());
$DBQueryObj->runSQL_Query();
echo $DBQueryObj->getRowsInJSON();