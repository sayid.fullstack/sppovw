<?php
include '../../../sppovw/vendor/autoload.php';
include '../../api/config/db_connection.php';

$maklumbalasObj = json_decode(file_get_contents("php://input"));
$DBQueryObj = new DBQuery($host, $username, $password, $database_name);

$DBCmd1 = new DBCommand($DBQueryObj);

//DumpJSON : {"peserta_id":"5007660487470331","jawapan_array":["Cardinal Health","Ventura Corporation LTD.","Home Sweet Homeopathics"],"peserta_nama":"Fred","kursus_id":1}
//var_dump($maklumbalasObj);exit;

//SIMPAN JAWAPAN
$query = <<<JAWAPAN
    INSERT INTO jawapan(peserta_id,soalan_id,jawapan) VALUES 
JAWAPAN;

$i = 1;
foreach ($maklumbalasObj->jawapan_array as $j){
    $query .= "('{$maklumbalasObj->peserta_id}',$j->soalan_id,'$j->jawapan'),";
    $i++;
}
$DBCmd1->executeCustomQueryCommand(rtrim($query, ','));

//SIMPAN MAKLUMBALAS
$DBCmd1->setINSERTintoTable('maklumbalas');
$DBCmd1->addINSERTcolumn('peserta_id',$maklumbalasObj->peserta_id,IFieldType::STRING_TYPE);
$DBCmd1->addINSERTcolumn('kursus_id',$maklumbalasObj->kursus_id,IFieldType::STRING_TYPE);
$DBCmd1->addINSERTcolumn('maklumbalas_comment',$maklumbalasObj->maklumbalas_comment,IFieldType::STRING_TYPE);
$DBCmd1->addINSERTcolumn('maklumbalas_date',date("Y-m-d H:i:s"),IFieldType::DATETIME_TYPE);
//echo $DBCmd1->getSQLstring();exit;
$DBCmd1->executeQueryCommand();

//SIMPAN BUTIRAN PESERTA
$DBCmd1->setINSERTintoTable('peserta');
$DBCmd1->addINSERTcolumn('peserta_id',$maklumbalasObj->peserta_id,IFieldType::STRING_TYPE);
$DBCmd1->addINSERTcolumn('peserta_nama',$maklumbalasObj->peserta_nama,IFieldType::STRING_TYPE);
$DBCmd1->executeQueryCommand();

//echo $DBCmd1->getSQLstring();exit;
$obj = new MagicObject();
if($DBCmd1->getAffectedRowCount() > 0) {
    $obj->dbStatus = 'success';
    $obj->message = 'Maklumbalas berjaya disimpan';
}
else {
    $obj->dbStatus = 'failed';
    $obj->message = 'Maklumbalas gagal disimpan.';
}
echo $obj->getJsonString();