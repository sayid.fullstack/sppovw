<?php
/* TODO : INCLUDE VENDOR LIBRARY */
require_once('./../../vendor/autoload.php');

/* TODO : INCLUDE DATABASE CONFIGURATION */
require_once ('./../config/db_connection.php');

/* TODO : INITIALIZE DB CONNECTION OBJECT */
$DBQueryObj = new DBQuery($host,$username,$password,$database_name);

/* TODO : CONSTRUCT SQL */
$sql = <<<SQL
SELECT kod, perihal
FROM _kod_skim
LIMIT 0, 20
SQL;

/* TODO : INSPECT SQL */
//echo $sql;exit;

/* TODO : QUERY DATABASE */
$DBQueryObj->setSQL_Statement($sql);
$DBQueryObj->runSQL_Query();

/* TODO : CONVERT RECORDSET TO JSON */
echo $DBQueryObj->getRowsInJSON();

