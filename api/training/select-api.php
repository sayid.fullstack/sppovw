<?php
/* TODO : INCLUDE VENDOR LIBRARY */
require_once('./../../vendor/autoload.php');

/* TODO : INCLUDE DATABASE CONFIGURATION */
require_once ('./../config/db_connection.php');

/* TODO : INITIALIZE DB CONNECTION OBJECT */
$DBQueryObj = new DBQuery($host,$username,$password,$database_name);

/* TODO : DEFINE VALUE */
$conditionStr='';

/* TODO : RETRIEVES INPUT PARAMETER */
if (count($_GET)>0) {

    $pagingObj = (object) $_GET;
    unset($_GET);

    /* Retrieve params if any */
    if(isset($pagingObj->nama)){
        if($conditionStr===''){
            $conditionStr=' WHERE ';
            $conditionStr.='nama LIKE \'%' . mysqli_real_escape_string($DBQueryObj->getLink(),$pagingObj->nama) . '%\'';
        }else{
            $conditionStr.=' AND nama LIKE \'%' . mysqli_real_escape_string($DBQueryObj->getLink(),$pagingObj->nama) . '%\'';
        }
    }
}

/* TODO : CONSTRUCT SQL */
$sql = <<<SQL
SELECT nama, emel
FROM pengguna $conditionStr
LIMIT 0, 20
SQL;

/* TODO : INSPECT SQL */
//echo $sql;exit;

/* TODO : QUERY DATABASE */
$DBQueryObj->setSQL_Statement($sql);
$DBQueryObj->runSQL_Query();

/* TODO : CONVERT RECORDSET TO JSON */
echo $DBQueryObj->getRowsInJSON();
