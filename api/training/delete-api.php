<?php
/* TODO : INCLUDE VENDOR LIBRARY */
require_once('./../../vendor/autoload.php');

/* TODO : INCLUDE DATABASE CONFIGURATION */
require_once ('./../config/db_connection.php');

/* TODO : RETRIEVES INPUT PARAMETER */
$inputObj = json_decode(file_get_contents("php://input"));

/* TODO : INITIALIZE DB CONNECTION OBJECT */
$DBQueryObj = new DBQuery($host,$username,$password,$database_name);

/* TODO : CONSTRUCT SQL */
$sql = <<<SQL
DELETE
FROM `maklumbalas`.`pengguna`
WHERE `IDpengguna` = '$inputObj->IDpengguna'
SQL;

/* TODO : INSPECT SQL */
//echo $sql;exit;

/* TODO : INITIALIZE DB COMMAND OBJECT */
$DBCommandObj = new DBCommand($DBQueryObj);

/* TODO : CONFIGURE DB COMMAND OBJECT */
$DBCommandObj->executeCustomQueryCommand($sql);

/* TODO : CREATE RESPONSE STATUS OBJECT */
$responseObj = new MagicObject();

/* TODO : CHECK DB OPERATION STATUS */

/* TODO : SAVE STATUS IN JSON */
if($DBCommandObj->getAffectedRowCount()>0){
    $responseObj->status ='OK';
    $responseObj->statusCode ='1';
}else{
    $responseObj->status ='FAILED';
    $responseObj->statusCode = $DBCommandObj->getAffectedRowCount();
}

/* TODO : SEND BACK STATUS TO CLIENT */
echo $responseObj->getJsonString();