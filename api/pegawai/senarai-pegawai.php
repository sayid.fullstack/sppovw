<?php
include '../../../sppovw/vendor/autoload.php';
include '../../api/config/db_connection.php';
$DBQueryObj = new DBQuery($host, $username, $password, $database_name);
//var_dump($pegawaiObj);
$sqlQueryObj = new SQLQuery($DBQueryObj);
$sqlQueryObj->setSELECT_INNER_JOIN_Query('pegawai','unit','lookup_unit','id');
$sqlQueryObj->addReturnField('pegawai.id as id');
$sqlQueryObj->addReturnField('nama');
$sqlQueryObj->addReturnField('lookup_unit.nama_unit as unit');
$sqlQueryObj->addReturnField('gelaran');
$DBQueryObj->setSQL_Statement($sqlQueryObj->getSQLQuery());
$DBQueryObj->runSQL_Query();
echo $DBQueryObj->getRowsInJSON();