'use strict';

/**
 * @ngdoc function
 * @name sppovwApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sppovwApp
 */
angular.module('sppovwApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
