'use strict';

/**
 * @ngdoc function
 * @name sppovwApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sppovwApp
 */
angular.module('sppovwApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
