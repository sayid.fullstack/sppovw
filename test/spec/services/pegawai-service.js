'use strict';

describe('Service: pegawaiService', function () {

  // load the service's module
  beforeEach(module('sppovwApp'));

  // instantiate service
  var pegawaiService;
  beforeEach(inject(function (_pegawaiService_) {
    pegawaiService = _pegawaiService_;
  }));

  it('should do something', function () {
    expect(!!pegawaiService).toBe(true);
  });

});
